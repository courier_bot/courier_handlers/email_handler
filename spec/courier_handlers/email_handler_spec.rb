# frozen_string_literal: true

require 'courier_handlers/email_handler'

RSpec.describe CourierHandlers::EmailHandler do
  let(:instance) { CourierHandlers::EmailHandler.new }

  describe '.deliver' do
    subject { instance.deliver(delivery_fields) }

    let(:delivery_fields) do
      {
        'from' => 'from@example.com',
        'to' => 'to@example.com',
        'subject' => 'Email Subject',
        'content' => 'Email content',
        'extra' => 'value'
      }
    end

    let(:normalized_delivery_fields) do
      {
        'from' => 'from@example.com',
        'to' => 'to@example.com',
        'subject' => 'Email Subject',
        'content' => 'Email content'
      }
    end

    it 'raises a `NotImplementedError`' do
      expect { subject }.to raise_error(NotImplementedError)
    end

    it 'normalizes the delivery fields' do
      expect(instance).to receive(:_deliver).with(normalized_delivery_fields)

      subject
    end
  end

  describe '.callback' do
    subject { CourierHandlers::EmailHandler.callback({}) }

    it 'raises a `NotImplementedError`' do
      expect { subject }.to raise_error(NotImplementedError)
    end
  end
end
