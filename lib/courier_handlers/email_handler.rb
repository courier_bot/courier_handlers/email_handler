# frozen_string_literal: true

require 'courier_handlers'

module CourierHandlers
  # Base handler for email handlers.
  class EmailHandler < BaseHandler
    # Handles the third-party service's callback and updates its delivery record.
    #
    # @params args [Hash] Third-party service's callback body.
    # @return (see super)
    def self.callback(_args)
      raise NotImplementedError
    end

    protected

    # Normalizes, validates, and cleans all of the specified fields.
    #
    # An email handler should at least accept the following fields:
    # * `from`: Email address(es) from which the email will originate.
    # * `to`: Email address(es) to which the email will be delivered.
    # * `cc`: Optional email address(es) to which the email will be carbon-copied.
    # * `bcc`: Optional email address(es) to which the email will be blindly carbon-copied.
    # * `subject`: Subject of the email.
    # * `content`: Content of the email.
    def normalize_delivery_fields(fields)
      allowed_keys = %w[from to cc bcc subject content]
      fields.select { |key, _| allowed_keys.include?(key) }
    end
  end
end
